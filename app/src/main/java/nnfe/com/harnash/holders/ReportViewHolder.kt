package nnfe.com.harnash.holders

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.support.v4.view.ViewCompat.animate
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.activity_main.view.*
import kotlinx.android.synthetic.main.expanded_report_list_item.view.*
import kotlinx.android.synthetic.main.fragment_reports_list.view.*
import nnfe.com.harnash.R
import nnfe.com.harnash.controllers.MainController
import nnfe.com.harnash.models.Report
import nnfe.com.harnash.models.ReportType

class ReportViewHolder(view: View, private val controller: MainController) :  RecyclerView.ViewHolder(view) {
    private var transportNumber = view.findViewById<TextView>(R.id.transport_number)
    private var reportType = view.findViewById<TextView>(R.id.report_type)
    private var numberOfReports = view.findViewById<TextView>(R.id.number_of_reports)
    private var busStop = view.findViewById<TextView>(R.id.bus_stop)
    private var wholeItem = view.findViewById<LinearLayout>(R.id.whole_report_item)

    fun bind(report: Report, fragmentView: View, mMapView: MapView) {
        when(report.reportType) {
            ReportType.Accident -> reportType.text = report.reportType.toString()
            ReportType.Delay -> reportType.text = report.reportType.toString() + ": " + report.delay
        }
        transportNumber.text = "Line " + report.transportNumber
        numberOfReports.text = report.numberOfReports.toString() + " of reports"
        busStop.text = report.stop

        fragmentView.cofirm_button.setOnClickListener { cofirm(report) }

        val animationDuration : Long = 700
        wholeItem.setOnClickListener {
            mMapView.getMapAsync { mMap ->
                mMap.clear()
                mMap.addMarker(MarkerOptions().position(report.location).title("Line " + report.transportNumber + ": " + report.from + " - " + report.to).snippet(report.stop))
                val cameraPosition = CameraPosition.Builder().target(report.location).zoom(20f).build()
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition))
            }



            fragmentView.expanded_item.apply {
                alpha = 0f
                visibility = View.VISIBLE

                animate()
                    .alpha(1f)
                    .setDuration(animationDuration)
                    .setListener(null)
            }

            fragmentView.reports_recycler_view.animate()
                .alpha(0f)
                .setDuration(animationDuration)
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        fragmentView.reports_recycler_view.visibility = View.GONE
                    }
                })
        }

        fragmentView.collapse_item_button.setOnClickListener { _ ->
            fragmentView.reports_recycler_view.apply {
                alpha = 0f
                visibility = View.VISIBLE

                animate()
                    .alpha(1f)
                    .setDuration(animationDuration)
                    .setListener(null)
            }

            fragmentView.expanded_item.animate()
                .alpha(0f)
                .setDuration(animationDuration)
                .setListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        fragmentView.expanded_item.visibility = View.GONE
                    }
                })
        }
    }

    private fun cofirm(report: Report) {
        controller.addReportAsync(report.createReportSendModel())
    }
}