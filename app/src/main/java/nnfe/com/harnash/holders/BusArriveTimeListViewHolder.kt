package nnfe.com.harnash.holders

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewTreeObserver
import android.widget.Button
import android.widget.MediaController
import android.widget.RelativeLayout
import android.widget.TextView
import nnfe.com.harnash.R
import nnfe.com.harnash.adapters.BasicListAdapter
import nnfe.com.harnash.controllers.MainController
import nnfe.com.harnash.interfaces.IObserver
import nnfe.com.harnash.models.LinePreference
import java.lang.IllegalArgumentException

class BusArriveTimeListViewHolder(itemView: View, controller: MainController, private val observer: IObserver) : BasicViewHolder(itemView, controller) {
    private val arriveTime: TextView = itemView.findViewById(R.id.bus_arrive_time)
    private val deleteButton: Button = itemView.findViewById(R.id.delete_lines_list_element_button)

    init {
        deleteButton.setOnClickListener { deleteItem() }
    }

    private fun deleteItem() {
        val timesRecycler = itemView.parent as RecyclerView
        val stationRelativeLayout = timesRecycler.parent as RelativeLayout
        val lineRecyclerView = stationRelativeLayout.parent as RecyclerView
        val lineRelativeLayout = lineRecyclerView.parent as RelativeLayout
        val topRecyclerView = lineRelativeLayout.parent as RecyclerView
        val timeText = itemView.findViewById<TextView>(R.id.bus_arrive_time).text
        val stationText = stationRelativeLayout.findViewById<TextView>(R.id.station_name).text.toString()
        val lineText = lineRelativeLayout.findViewById<TextView>(R.id.bus_number).text.toString()

        val hour = timeText.subSequence(0, 2).toString().toInt()
        val minute = timeText.subSequence(3, 5).toString().toInt()
        observer.deleteElement(LinePreference(lineText, hour, minute, stationText), topRecyclerView.adapter as BasicListAdapter)
    }

    override fun bind(vararg listElements: Any) {
        when (val time = listElements[0]) {
            is String -> {
                arriveTime.text = time
            }
            else -> {
                throw IllegalArgumentException()
            }
        }
    }
}