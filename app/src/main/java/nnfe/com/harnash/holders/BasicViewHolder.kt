package nnfe.com.harnash.holders

import android.support.v7.widget.RecyclerView
import android.view.View
import nnfe.com.harnash.controllers.MainController

abstract class BasicViewHolder(itemView: View, controller: MainController) : RecyclerView.ViewHolder(itemView) {
    abstract fun bind(vararg listElements: Any)
}