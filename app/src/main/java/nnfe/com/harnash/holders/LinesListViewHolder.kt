package nnfe.com.harnash.holders

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import nnfe.com.harnash.models.LinesListElement
import nnfe.com.harnash.R
import nnfe.com.harnash.adapters.BasicListAdapter
import nnfe.com.harnash.controllers.MainController
import nnfe.com.harnash.models.AdapterType

class LinesListViewHolder(itemView: View, private val controller: MainController) : BasicViewHolder(itemView, controller) {
    private val busNumber: TextView = itemView.findViewById(R.id.bus_number)
    private val stationsList: RecyclerView = itemView.findViewById(R.id.bus_stations_list)

    override fun bind(vararg listElements: Any) {
        when (val element = listElements[0]) {
            is LinesListElement -> {
                busNumber.text = element.lineNumber
                stationsList.layoutManager = LinearLayoutManager(itemView.context)
                val adapter =
                    BasicListAdapter(element.selectedRoutes, AdapterType.BusStationsList, controller)
                stationsList.adapter = adapter
            }
            else -> {
                throw IllegalArgumentException()
            }
        }
    }
}