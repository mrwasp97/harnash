package nnfe.com.harnash.holders

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.TextView
import nnfe.com.harnash.R
import nnfe.com.harnash.adapters.BasicListAdapter
import nnfe.com.harnash.controllers.MainController
import nnfe.com.harnash.models.AdapterType
import java.lang.IllegalArgumentException

class BusStationsListViewHolder(itemView: View, private val controller: MainController) : BasicViewHolder(itemView, controller) {
    private val stationName: TextView = itemView.findViewById(R.id.station_name)
    private val arriveTimeList: RecyclerView = itemView.findViewById(R.id.bus_arrive_time_list)

    override fun bind(vararg listElements: Any) {
        when (val pair = listElements[0]) {
            is Pair<*, *> -> {
                stationName.text = pair.first as String
                arriveTimeList.layoutManager = LinearLayoutManager(itemView.context)
                val adapter = BasicListAdapter(
                    pair.second as ArrayList<Any>,
                    AdapterType.BusArriveTimeList,
                    controller
                )
                arriveTimeList.adapter = adapter
            }
            else -> {
                throw IllegalArgumentException()
            }
        }
    }
}