package com.example.androidclassproject.neighborlyhelp.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import nnfe.com.harnash.R
import nnfe.com.harnash.controllers.MainController
import nnfe.com.harnash.holders.ReportViewHolder
import nnfe.com.harnash.models.Report
import java.util.*

class ReportsAdapter(val activityView: View, val mMapView: MapView, private val controller: MainController): RecyclerView.Adapter<ReportViewHolder>() {
    val data: MutableList<Report>

    init {
        data = ArrayList()
    }

    fun swapData(newData: List<Report>) {
        data.clear()
        data.addAll(newData)
        notifyDataSetChanged()
    }

    fun removeAt(position: Int) {
        data.removeAt(position)
        notifyItemRemoved(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReportViewHolder {
        val context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.report_list_item, parent, false)
        return ReportViewHolder(view, controller)
    }

    override fun onBindViewHolder(holder: ReportViewHolder, position: Int) = holder.bind(data[position], activityView, mMapView)
    override fun getItemCount() = data.size
}