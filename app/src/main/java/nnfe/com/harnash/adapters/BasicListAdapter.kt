package nnfe.com.harnash.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import nnfe.com.harnash.*
import nnfe.com.harnash.controllers.MainController
import nnfe.com.harnash.holders.BasicViewHolder
import nnfe.com.harnash.holders.BusArriveTimeListViewHolder
import nnfe.com.harnash.holders.BusStationsListViewHolder
import nnfe.com.harnash.holders.LinesListViewHolder
import nnfe.com.harnash.interfaces.IObserver
import nnfe.com.harnash.models.AdapterType
import nnfe.com.harnash.models.LinePreference
import nnfe.com.harnash.models.LinesListElement
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class BasicListAdapter(private var list: ArrayList<Any>, private val adapterType: AdapterType, private val controller: MainController) : RecyclerView.Adapter<BasicViewHolder>(), IObserver {


    override fun deleteElement(preference: LinePreference, topAdapter: BasicListAdapter) {

        doAsync {
            controller.removePreference(preference)
            val newList = controller.getAllPreferences()
            uiThread {
                topAdapter.swapData(LinesListElement.convertLinePreferenceToLinesList(newList))
                topAdapter.notifyDataSetChanged()
            }
        }
    }

    fun swapData(newData: ArrayList<Any>) {
        list.clear()
        list.addAll(newData)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, p1: Int): BasicViewHolder {
        when(adapterType) {

            AdapterType.LinesList -> {
                val view = LayoutInflater.from(viewGroup.context)
                    .inflate(R.layout.lines_list_element, viewGroup, false)
                return LinesListViewHolder(view, controller)
            }
            AdapterType.BusStationsList -> {
                val view = LayoutInflater.from(viewGroup.context)
                    .inflate(R.layout.bus_stations_list_element, viewGroup, false)
                return BusStationsListViewHolder(view, controller)
            }
            AdapterType.BusArriveTimeList -> {
                val view = LayoutInflater.from(viewGroup.context)
                    .inflate(R.layout.bus_arrive_time_list_element, viewGroup, false)
                return BusArriveTimeListViewHolder(view, controller, this)
            }
        }
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onBindViewHolder(viewHolder: BasicViewHolder, elementPostition: Int) {
        viewHolder.bind(list[elementPostition])
    }
}