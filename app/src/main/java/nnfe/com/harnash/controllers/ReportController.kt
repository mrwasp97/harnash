package nnfe.com.harnash.controllers

import android.content.Context
import com.google.gson.internal.LinkedTreeMap
import nnfe.com.harnash.adapters.BasicListAdapter
import nnfe.com.harnash.models.Report
import nnfe.com.harnash.models.ReportSendModel

class ReportController(context: Context, token: String) : BasicController(context) {

    private val addPath = "reports"
    private val getPath = "reports/$token"

    fun addReport(report: ReportSendModel) {
        httpClient.post<ReportSendModel, Any>(addPath, report)
    }

    fun addReportAsync(report: ReportSendModel) {
        val func : ()->Unit = { httpClient.post<ReportSendModel, Any>(addPath, report); }
        executeAsync(func)
    }

    fun getAllReports() : List<Report> {
        val reports : List<LinkedTreeMap<String, Any>> = httpClient.get(getPath)
        return reports.map { treeMap -> Report(treeMap) }
    }

    fun getAllReportsAsync(adapter : BasicListAdapter) {
        val func : ()-> List<LinkedTreeMap<String, Any>> = { httpClient.get(getPath); }
        val update : (reports :  List<LinkedTreeMap<String, Any>>)->Unit = {
            val convertedList = it.map { treeMap -> Report(treeMap) }
            adapter.swapData(ArrayList(convertedList))
        }
        executeAndUpdateAsync(func, update)
    }
}