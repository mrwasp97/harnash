package nnfe.com.harnash.controllers

import android.content.Context

class RegisterController(context: Context, private val token: String) : BasicController(context){

    private val registerPath = "register"

    fun register() {
        httpClient.put<String, Any>(registerPath, token)
    }

    fun registerAsync() {
        val func : ()->Unit = { httpClient.put<String, Any>(registerPath, token); }
        executeAsync(func)
    }
}