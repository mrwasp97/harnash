package nnfe.com.harnash.controllers

import android.content.Context
import com.google.gson.internal.LinkedTreeMap
import nnfe.com.harnash.adapters.BasicListAdapter
import nnfe.com.harnash.models.LinePreference
import nnfe.com.harnash.models.LinesListElement

class PreferenceController(context: Context, token: String) : BasicController(context) {

    private val addPath = "preferences/$token"
    private val removePath = "preferences/$token"
    private val getPath = "preferences/$token"

    fun addPreference(preference: LinePreference) {
        httpClient.put<LinePreference, Any>(addPath, preference)
    }

    fun addPreferenceAsync(preference: LinePreference) {
        val func : ()->Unit = { httpClient.put<LinePreference, Any>(addPath, preference); }
        executeAsync(func)
    }

    fun removePreference(preference: LinePreference) {
        httpClient.delete<LinePreference, Any>(removePath, preference)
    }

    fun removePreferenceAsync(preference: LinePreference) {
        val func : ()->Unit = { httpClient.delete<LinePreference, Any>(removePath, preference); }
        executeAsync(func)
    }

    fun getAllPreferences() : List<LinePreference> {
        val preferences : List<LinkedTreeMap<String, Any>> = httpClient.get(getPath)
        return preferences.map { treeMap -> LinePreference(treeMap) }
    }

    fun getAllPreferencesAsync(adapter : BasicListAdapter) {
        val func : ()->List<LinkedTreeMap<String, Any>> = { httpClient.get(getPath); }
        val update : (linePreferences : List<LinkedTreeMap<String, Any>>)->Unit = {
            val convertedList = LinesListElement.convertLinePreferenceToLinesList(it.map { line -> LinePreference(line) })
            adapter.swapData(convertedList)
        }
        executeAndUpdateAsync(func, update)
    }
}