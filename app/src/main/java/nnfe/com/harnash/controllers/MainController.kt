package nnfe.com.harnash.controllers

import android.content.Context
import com.google.gson.internal.LinkedTreeMap
import nnfe.com.harnash.adapters.BasicListAdapter
import nnfe.com.harnash.models.*

class MainController(context: Context, token: String) {

    private var registerController : RegisterController = RegisterController(context, token)
    private var preferenceController : PreferenceController = PreferenceController(context, token)
    private var reportController : ReportController = ReportController(context, token)
    private var lineController : LineController = LineController(context)

    fun register() {
        registerController.register()
    }

    fun registerAsync() {
        registerController.registerAsync()
    }

    fun addPreference(preference: LinePreference) {
        preferenceController.addPreference(preference)
    }

    fun addPreferenceAsync(preference: LinePreference) {
        preferenceController.addPreferenceAsync(preference)
    }

    fun removePreference(preference: LinePreference) {
        preferenceController.removePreference(preference)
    }

    fun removePreferenceAsync(preference: LinePreference) {
        preferenceController.removePreferenceAsync(preference)
    }

    fun getAllPreferences() : List<LinePreference> {
        return preferenceController.getAllPreferences()
    }

    fun getAllPreferencesAsync(adapter : BasicListAdapter) {
        preferenceController.getAllPreferencesAsync(adapter)
    }


    fun addReport(report: ReportSendModel) {
        reportController.addReport(report)
    }

    fun addReportAsync(report: ReportSendModel) {
        reportController.addReportAsync(report)
    }

    fun getAllReports() : List<Report> {
        return reportController.getAllReports()
    }

    fun getAllReportsAsync(adapter : BasicListAdapter) {
        reportController.getAllReportsAsync(adapter)
    }

    fun getTimeInfos(line: String, stop: String) : List<TimeInfo> {
        return lineController.getTimeInfos(line, stop)
    }

    fun getTimeInfosAsync(line: String, stop: String, adapter : BasicListAdapter) {
        return lineController.getTimeInfosAsync(line, stop, adapter)
    }

    fun getAllStops(line: String, from: String, to: String) : List<Stop> {
        return lineController.getAllStops(line, from, to)
    }

    fun getAllStopsAsync(line: Int, from: String, to: String, adapter : BasicListAdapter) {
        lineController.getAllStopsAsync(line, from, to, adapter)
    }

    fun getAllLines() : List<Line> {
        return lineController.getAllLines()
    }

    fun getAllLinesAsync(adapter : BasicListAdapter) {
        lineController.getAllLinesAsync(adapter)
    }
}