package nnfe.com.harnash.controllers

import android.content.Context
import android.widget.Toast
import nnfe.com.harnash.R
import nnfe.com.harnash.helpers.HttpClient
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

open class BasicController(protected val context: Context) {

    protected var httpClient: HttpClient = HttpClient(context.getString(R.string.host))

    protected fun executeAsync(func: () -> Unit, successDescription: String = "") {
        doAsync {
            try {
                func()
                uiThread {
                    if (successDescription != "") {
                        Toast.makeText(context, successDescription, Toast.LENGTH_SHORT).show()
                    }
                }
            } catch (e: Exception) {
                uiThread {
                    Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    protected fun <RES> executeAndUpdateAsync(func: () -> RES, update: (res: RES) -> Unit, successDescription: String = "") {
        doAsync {
            try {
                val result = func()
                uiThread {
                    update(result)
                    if (successDescription != "") {
                        Toast.makeText(context, successDescription, Toast.LENGTH_SHORT).show()
                    }
                }
            } catch (e: Exception) {
                uiThread {
                    Toast.makeText(context, e.message, Toast.LENGTH_SHORT).show()
                }
            }
        }
    }
}