package nnfe.com.harnash.controllers

import android.content.Context
import com.google.gson.internal.LinkedTreeMap
import nnfe.com.harnash.adapters.BasicListAdapter
import nnfe.com.harnash.models.Line
import nnfe.com.harnash.models.Stop
import nnfe.com.harnash.models.TimeInfo

class LineController(context: Context) : BasicController(context) {

    private val getPath = "lines"

    fun getTimeInfos(line: String, stop: String) : List<TimeInfo> {
        val timeInfos : List<LinkedTreeMap<String, Any>> = httpClient.get("$getPath/$line/$stop")
        return timeInfos.map { treeMap -> TimeInfo(treeMap) }
    }

    fun getTimeInfosAsync(line: String, stop: String, adapter : BasicListAdapter) {
        val func : ()->List<LinkedTreeMap<String, Any>> = { httpClient.get("$getPath/$line/$stop"); }
        val update : (reports : List<LinkedTreeMap<String, Any>>)->Unit = {
            val convertedList = it.map { treeMap -> TimeInfo(treeMap) }
            adapter.swapData(ArrayList(convertedList))
        }
        executeAndUpdateAsync(func, update)
    }

    fun getAllStops(line: String, from: String, to: String) : List<Stop> {
        val stops : List<LinkedTreeMap<String, Any>> = httpClient.get("$getPath/$line?from=$from&to=$to")
        return stops.map { treeMap -> Stop(treeMap) }
    }

    fun getAllStopsAsync(line: Int, from: String, to: String, adapter : BasicListAdapter) {
        val func : ()->List<LinkedTreeMap<String, Any>> = { httpClient.get("$getPath/$line?from=$from&to=$to"); }
        val update : (reports : List<LinkedTreeMap<String, Any>>)->Unit = {
            val convertedList = it.map { treeMap -> Stop(treeMap) }
            adapter.swapData(ArrayList(convertedList))
        }
        executeAndUpdateAsync(func, update)
    }

    fun getAllLines() : List<Line> {
        val stops : List<LinkedTreeMap<String, Any>> = httpClient.get(getPath)
        return stops.map { treeMap -> Line(treeMap) }
    }

    fun getAllLinesAsync(adapter : BasicListAdapter) {
        val func : ()->List<LinkedTreeMap<String, Any>> = { httpClient.get(getPath); }
        val update : (reports : List<LinkedTreeMap<String, Any>>)->Unit = {
            val convertedList = it.map { treeMap -> Line(treeMap) }
            adapter.swapData(ArrayList(convertedList))
        }
        executeAndUpdateAsync(func, update)
    }

}