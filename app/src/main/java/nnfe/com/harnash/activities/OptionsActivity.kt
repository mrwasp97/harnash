package nnfe.com.harnash.activities

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import nnfe.com.harnash.R
import nnfe.com.harnash.fragments.AddNewPreferenceFragment
import nnfe.com.harnash.fragments.NewEventFragment
import nnfe.com.harnash.fragments.OptionsFragment
import nnfe.com.harnash.interfaces.OnOptionsFragmentListener
import nnfe.com.harnash.models.ReportType

class OptionsActivity : AppCompatActivity(), OnOptionsFragmentListener {
    override fun showPreferenceList() {
        val ft = supportFragmentManager
        ft.popBackStack()
    }

    override fun addNewPreference() {
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.options_fragments_container, AddNewPreferenceFragment.newInstance()).addToBackStack(null)
        ft.commit()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_options)

        val ft = supportFragmentManager.beginTransaction()
        ft.add(R.id.options_fragments_container, OptionsFragment.newInstance())
        ft.commit()
    }
}
