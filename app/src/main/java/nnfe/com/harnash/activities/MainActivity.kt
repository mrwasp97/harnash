package nnfe.com.harnash.activities

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import nnfe.com.harnash.R
import nnfe.com.harnash.controllers.MainController
import nnfe.com.harnash.fragments.ChooseReportTypeFragment
import nnfe.com.harnash.fragments.NewEventFragment
import nnfe.com.harnash.fragments.ReportsListFragment
import nnfe.com.harnash.interfaces.OnFragmentInteractionListener
import nnfe.com.harnash.models.ReportType
import org.jetbrains.anko.doAsync


class MainActivity : AppCompatActivity(), OnFragmentInteractionListener {
    private lateinit var controller: MainController

    override fun addNewEvent(reportType: ReportType) {
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.fragments_container, NewEventFragment.newInstance(reportType)).addToBackStack(null)
        ft.commit()
    }

    override fun chooseReportType() {
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.fragments_container, ChooseReportTypeFragment.newInstance()).addToBackStack(null)
        ft.commit()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupController()
        val ft = supportFragmentManager.beginTransaction()
        ft.add(R.id.fragments_container, ReportsListFragment.newInstance())
        ft.commit()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.preferences_menu_item) {
            val intent = Intent(applicationContext, OptionsActivity::class.java)
            startActivity(intent)
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main_activity_menu, menu)
        return true
    }

    private fun setupController() {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("Token: ", "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                val token = task.result?.token

                if (token != null) {
                    controller = MainController(this.applicationContext, token)

                    doAsync {
                        controller.register()
                    }

                } else {
                    Toast.makeText(this.applicationContext, "Fucking error, token not found!!!!", Toast.LENGTH_SHORT).show()
                }
            })
    }
}
