package nnfe.com.harnash.interfaces

import nnfe.com.harnash.models.ReportType

interface OnFragmentInteractionListener {
    fun chooseReportType()
    fun addNewEvent(reportType: ReportType)
}