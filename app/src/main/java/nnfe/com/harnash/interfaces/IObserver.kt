package nnfe.com.harnash.interfaces

import nnfe.com.harnash.adapters.BasicListAdapter
import nnfe.com.harnash.models.LinePreference

interface IObserver {
    fun deleteElement(preference: LinePreference, topAdapter: BasicListAdapter)
}