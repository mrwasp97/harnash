package nnfe.com.harnash.interfaces

interface OnOptionsFragmentListener {
    fun addNewPreference()
    fun showPreferenceList()
}