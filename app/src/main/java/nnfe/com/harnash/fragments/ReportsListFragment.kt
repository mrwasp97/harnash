package nnfe.com.harnash.fragments

import android.content.Context
import android.os.Bundle
import android.os.Debug
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.androidclassproject.neighborlyhelp.adapters.ReportsAdapter
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.MapsInitializer
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.expanded_report_list_item.view.*
import kotlinx.android.synthetic.main.fragment_reports_list.*
import kotlinx.android.synthetic.main.fragment_reports_list.view.*
import nnfe.com.harnash.R
import nnfe.com.harnash.adapters.BasicListAdapter
import nnfe.com.harnash.controllers.MainController
import nnfe.com.harnash.interfaces.OnFragmentInteractionListener
import nnfe.com.harnash.models.AdapterType
import nnfe.com.harnash.models.LinesListElement
import nnfe.com.harnash.models.Report
import nnfe.com.harnash.models.ReportType
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.doAsyncResult
import org.jetbrains.anko.uiThread
import java.util.*
import android.support.v4.widget.SwipeRefreshLayout




class ReportsListFragment : Fragment() {
    private var listener: OnFragmentInteractionListener? = null
    private val allReports: MutableList<Report> = ArrayList()
    lateinit var mMapView: MapView
    private lateinit var googleMap: GoogleMap
    private lateinit var controller: MainController
    private lateinit var linesList: RecyclerView
    private lateinit var adapter: ReportsAdapter
    private lateinit var rootView: View


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_reports_list, container, false)
        setupController()
        linesList = rootView.findViewById(R.id.reports_recycler_view)

        mMapView = rootView.map_view
        mMapView.onCreate(savedInstanceState)
        mMapView.onResume() // needed to get the map to display immediately

        try {
            MapsInitializer.initialize(activity!!.applicationContext)
        } catch (e: Exception) {
            e.printStackTrace()
        }


        rootView.add_report_button.setOnClickListener {
            run {
                listener?.chooseReportType()
            }
        }

        rootView.swipe_refresh_layout.setOnRefreshListener {
            doAsync {
                val newList = controller.getAllReports()

                uiThread {
                    linesList.layoutManager = LinearLayoutManager(activity!!.applicationContext)
                    adapter.swapData(newList)
                    linesList.adapter = adapter
                }
            }.doAsyncResult {
                rootView.swipe_refresh_layout.isRefreshing = false
            }
        }



        return rootView
    }

    private fun setupController() {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("Token: ", "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                val token = task.result?.token

                if (token != null) {
                    controller = MainController(activity!!.applicationContext, token)

                    rootView.reports_recycler_view.layoutManager = LinearLayoutManager(activity)
                    adapter = ReportsAdapter(rootView, mMapView, controller)
                    rootView.reports_recycler_view.adapter = adapter
                    doAsync {
                        val newList = controller.getAllReports()

                        uiThread {
                            linesList.layoutManager = LinearLayoutManager(activity!!.applicationContext)
                            adapter.swapData(newList)
                            linesList.adapter = adapter
                        }
                    }

                } else {
                    Toast.makeText(activity!!.applicationContext, "Fucking error, token not found!!!!", Toast.LENGTH_SHORT).show()
                }
            })
    }

    override fun onResume() {
        super.onResume()
        mMapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mMapView.onPause()
    }

    override fun onDestroy() {
        super.onDestroy()
        mMapView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mMapView.onLowMemory()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            ReportsListFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}
