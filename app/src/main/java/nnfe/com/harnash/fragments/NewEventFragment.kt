package nnfe.com.harnash.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.fragment_new_event.view.*
import nnfe.com.harnash.R
import nnfe.com.harnash.controllers.MainController
import nnfe.com.harnash.interfaces.OnFragmentInteractionListener
import nnfe.com.harnash.models.ReportSendModel
import nnfe.com.harnash.models.ReportType
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.doAsyncResult
import org.jetbrains.anko.uiThread


class NewEventFragment : Fragment(), AdapterView.OnItemSelectedListener {
    private var listener: OnFragmentInteractionListener? = null
    private lateinit var rootView: View
    private lateinit var reportType: ReportType
    private var reportSendModel: ReportSendModel = ReportSendModel()
    private lateinit var controller: MainController

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        rootView = inflater.inflate(R.layout.fragment_new_event, container, false)
        setupController()
        reportSendModel.reportType = reportType
        when(reportType) {
            ReportType.Delay -> {
                rootView.delay_item.visibility = View.VISIBLE
            }
            ReportType.Accident -> {
                reportSendModel.delay = -1
                rootView.delay_item.visibility = View.GONE
            }
        }

        rootView.event_bus_stop.onItemSelectedListener = this
        rootView.event_time.onItemSelectedListener = this



        ArrayAdapter.createFromResource(
            activity!!.applicationContext,
            R.array.delays,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            // Apply the adapter to the spinner
            rootView.event_delay.adapter = adapter
        }
        rootView.event_delay.onItemSelectedListener = this

        rootView.report_button.setOnClickListener { _ ->
            run {
                controller.addReportAsync(reportSendModel)
                activity!!.supportFragmentManager.popBackStack()
            }
        }

        return rootView
    }

    private fun setupController() {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("Token: ", "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                val token = task.result?.token

                if (token != null) {
                    controller = MainController(activity!!.applicationContext, token)

                    doAsync {
                        val newList = controller.getAllLines().map { e -> e.toString() }

                        uiThread {
                            ArrayAdapter(this@NewEventFragment.context!!, android.R.layout.simple_spinner_item, newList)
                            .also { adapter ->
                                // Specify the layout to use when the list of choices appears
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                                rootView.event_bus_line.adapter = adapter
                                rootView.event_bus_line.onItemSelectedListener = this@NewEventFragment
                            }
                        }
                    }

                } else {
                    Toast.makeText(activity, "Fucking error, token not found!!!!", Toast.LENGTH_SHORT).show()
                }
            })
    }

    override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
        when(parent.id) {
            R.id.event_bus_line -> {
                val lineInParts = (parent.getItemAtPosition(pos) as String).split(":")
                reportSendModel.line = lineInParts[0]
                reportSendModel.from = lineInParts[1]
                reportSendModel.to = lineInParts[2]
                doAsync {
                    val newList = controller.getAllStops(lineInParts[0], lineInParts[1], lineInParts[2]).map { e -> e.toString() }

                    uiThread {
                        ArrayAdapter(this@NewEventFragment.context!!, android.R.layout.simple_spinner_item, newList)
                            .also { adapter ->
                                // Specify the layout to use when the list of choices appears
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                                rootView.event_bus_stop.adapter = adapter
                            }
                    }
                }
            }
            R.id.event_bus_stop -> {
                reportSendModel.stop = parent.getItemAtPosition(pos) as String
                doAsync {
                    val newList = controller.getTimeInfos(reportSendModel.line, reportSendModel.stop).map {e -> e.toString() }
                    uiThread {
                        ArrayAdapter(this@NewEventFragment.context!!, android.R.layout.simple_spinner_item, newList)
                            .also { adapter ->
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                                rootView.event_time.adapter = adapter
                            }
                    }
                }
            }
            R.id.event_time -> {
                val time = parent.getItemAtPosition(pos) as String
                reportSendModel.hour = time.split(':')[0].toInt()
                reportSendModel.minute = time.split(':')[1].toInt()
            }
            R.id.event_delay -> {
                reportSendModel.delay = (parent.getItemAtPosition(pos) as String).toInt()
            }
        }
    }
    override fun onNothingSelected(parent: AdapterView<*>?) {
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }


    companion object {
        @JvmStatic
        fun newInstance(rt: ReportType ) =
            NewEventFragment().apply {
                arguments = Bundle().apply {
                    reportType = rt
                }
            }
    }
}
