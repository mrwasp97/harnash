package nnfe.com.harnash.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_choose_report_type.view.*
import nnfe.com.harnash.R
import nnfe.com.harnash.interfaces.OnFragmentInteractionListener
import nnfe.com.harnash.models.ReportType

class ChooseReportTypeFragment : Fragment() {
    private var listener: OnFragmentInteractionListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var rootView =  inflater.inflate(R.layout.fragment_choose_report_type, container, false)
        rootView.delay_type_button.setOnClickListener { _ ->
            run {
                listener?.addNewEvent(ReportType.Delay)
            }
        }

        rootView.accident_type_button.setOnClickListener { _ ->
            run {
                listener?.addNewEvent(ReportType.Accident)
            }
        }
        return rootView
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            ChooseReportTypeFragment().apply {
                arguments = Bundle().apply {
                }
            }
    }
}
