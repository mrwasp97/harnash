package nnfe.com.harnash.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.android.synthetic.main.fragment_new_event.view.*
import nnfe.com.harnash.R
import nnfe.com.harnash.controllers.MainController
import nnfe.com.harnash.interfaces.OnOptionsFragmentListener
import nnfe.com.harnash.models.LinePreference
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread


class AddNewPreferenceFragment : Fragment(),  AdapterView.OnItemSelectedListener {
    private var listener: OnOptionsFragmentListener? = null
    private lateinit var rootView: View
    private lateinit var controller: MainController
    private var preference: LinePreference = LinePreference("empty", -1, -1, "empty")

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setupController()
        rootView = inflater.inflate(R.layout.fragment_new_event, container, false)
        val button = rootView.findViewById<Button>(R.id.report_button)
        button.text = getString(R.string.add_preference)
        button.setOnClickListener { addNewPreference() }

        rootView.event_bus_stop.onItemSelectedListener = this
        rootView.event_time.onItemSelectedListener = this

        return rootView
    }


    override fun onItemSelected(parent: AdapterView<*>, view: View, pos: Int, id: Long) {
        when(parent.id) {
            R.id.event_bus_line -> {
                val lineInParts = (parent.getItemAtPosition(pos) as String).split(":")
                preference.line = lineInParts[0]
                doAsync {
                    val newList = controller.getAllStops(lineInParts[0], lineInParts[1], lineInParts[2]).map { e -> e.toString() }

                    uiThread {
                        ArrayAdapter(this@AddNewPreferenceFragment.context!!, android.R.layout.simple_spinner_item, newList)
                            .also { adapter ->
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                                rootView.event_bus_stop.adapter = adapter
                            }
                    }
                }
            }
            R.id.event_bus_stop -> {
                preference.busStation = parent.getItemAtPosition(pos) as String
                doAsync {
                    val newList = controller.getTimeInfos(preference.line, preference.busStation).map { e -> e.toString() }
                    uiThread {
                        ArrayAdapter(this@AddNewPreferenceFragment.context!!, android.R.layout.simple_spinner_item, newList)
                            .also { adapter ->
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                                rootView.event_time.adapter = adapter
                            }
                    }
                }
            }
            R.id.event_time -> {
                val time = parent.getItemAtPosition(pos) as String
                preference.hour = time.split(':')[0].toInt()
                preference.minute = time.split(':')[1].toInt()
            }
        }
    }
    override fun onNothingSelected(parent: AdapterView<*>?) {
    }

    private fun setupController() {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("Token: ", "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                val token = task.result?.token

                if (token != null) {
                    controller = MainController(activity!!.applicationContext, token)

                    doAsync {
                        val newList = controller.getAllLines().map { e -> e.toString() }

                        uiThread {
                            ArrayAdapter(this@AddNewPreferenceFragment.context!!, android.R.layout.simple_spinner_item, newList)
                                .also { adapter ->
                                    // Specify the layout to use when the list of choices appears
                                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                                    rootView.event_bus_line.adapter = adapter
                                    rootView.event_bus_line.onItemSelectedListener = this@AddNewPreferenceFragment
                                }
                        }
                    }

                } else {
                    Toast.makeText(activity!!.applicationContext, "Fucking error, token not found!!!!", Toast.LENGTH_SHORT).show()
                }
            })
    }

    private fun addNewPreference() {
        controller.addPreferenceAsync(preference)
        listener!!.showPreferenceList()
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnOptionsFragmentListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    companion object {
        @JvmStatic
        fun newInstance() =
            AddNewPreferenceFragment().apply {
            }
    }
}
