package nnfe.com.harnash.fragments

import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import nnfe.com.harnash.R
import nnfe.com.harnash.adapters.BasicListAdapter
import nnfe.com.harnash.controllers.MainController
import nnfe.com.harnash.interfaces.OnFragmentInteractionListener
import nnfe.com.harnash.interfaces.OnOptionsFragmentListener
import nnfe.com.harnash.models.AdapterType
import nnfe.com.harnash.models.LinePreference
import nnfe.com.harnash.models.LinesListElement
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.find
import org.jetbrains.anko.uiThread


class OptionsFragment : Fragment() {
    private var listener: OnOptionsFragmentListener? = null
    private lateinit var controller: MainController
    private lateinit var linesList: RecyclerView


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val rootView = inflater.inflate(R.layout.fragment_options, container, false)
        linesList = rootView.findViewById(R.id.selected_lines_list)
        setupController()
        val button = rootView.findViewById<Button>(R.id.add_button)
        button.setOnClickListener { listener!!.addNewPreference() }

        return rootView
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnOptionsFragmentListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    private fun setupController() {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("Token: ", "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                val token = task.result?.token

                if (token != null) {
                    controller = MainController(activity!!.applicationContext, token)


                    doAsync {
                        val newList = controller.getAllPreferences()

                        uiThread {
                            linesList.layoutManager = LinearLayoutManager(activity!!.applicationContext)
                            val adapter = BasicListAdapter(LinesListElement.convertLinePreferenceToLinesList(newList), AdapterType.LinesList, controller)

                            linesList.adapter = adapter
                            adapter.notifyDataSetChanged()
                        }
                    }

                } else {
                    Toast.makeText(activity!!.applicationContext, "Fucking error, token not found!!!!", Toast.LENGTH_SHORT).show()
                }
            })
    }



    companion object {
        @JvmStatic
        fun newInstance() =
            OptionsFragment().apply {
            }
    }
}
