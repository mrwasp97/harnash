package nnfe.com.harnash.exceptions

import java.lang.Exception

class ResponseException(code: Int) : Exception("Response Code: $code")