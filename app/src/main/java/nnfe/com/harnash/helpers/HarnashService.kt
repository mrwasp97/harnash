package nnfe.com.harnash.helpers

import android.util.Log
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage

class HarnashService : FirebaseMessagingService() {

    override fun onMessageReceived(remoteMessage: RemoteMessage?) {
        Log.d("FROM", "From: ${remoteMessage?.from}")

        var notification = remoteMessage?.notification

        var data = remoteMessage?.data
        var keys = data?.keys


        // Check if message contains a data payload.
        remoteMessage?.data?.isNotEmpty()?.let {
            Log.d("MSG DATA", "Message data payload: " + remoteMessage.data)
        }

        remoteMessage?.notification?.let {
            Log.d("xd", "Message Notification Body: ${it.body}")
        }
    }
}