package nnfe.com.harnash.helpers

import android.util.Log
import com.google.gson.GsonBuilder
import com.squareup.okhttp.*
import nnfe.com.harnash.exceptions.ResponseException

class HttpClient(val host: String) {

    private val jsonType = MediaType.parse("application/json; charset=utf-8")
    private val client = OkHttpClient()
    private val gson = GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss").create()

    inline fun <reified RES> get(path: String) : RES {
        val request = Request.Builder().url(host + path).build()
        val content = executeRequest(request)
        return parseContent(content, RES::class.java)
    }

    inline fun <REQ, reified RES> delete(path: String, data: REQ) : RES {
        val requestBody = prepareRequestBody(data)
        val request = Request.Builder().url(host + path).delete(requestBody).build()
        val content = executeRequest(request)
        return parseContent(content, RES::class.java)
    }

    inline fun <REQ, reified RES> post(path: String, data: REQ) : RES {
        val requestBody = prepareRequestBody(data)
        val request = Request.Builder().url(host + path).post(requestBody).build()
        val content = executeRequest(request)
        return parseContent(content, RES::class.java)
    }

    inline fun <REQ, reified RES> put(path: String, data: REQ) : RES {
        val requestBody = prepareRequestBody(data)
        val request = Request.Builder().url(host + path).put(requestBody).build()
        val content = executeRequest(request)
        return parseContent(content, RES::class.java)
    }

    fun <REQ> prepareRequestBody(data: REQ) : RequestBody {
        val requestContent = gson.toJson(data)
        return RequestBody.create(jsonType, requestContent)
    }

    fun executeRequest(request: Request) : String {
        val response = client.newCall(request).execute()

        if (response.code() != 200 && response.code() != 409) {
            throw ResponseException(response.code())
        }

        Log.d("Response code: ", response.code().toString())
        val content = response.body().string()
        Log.d(null, content)
        return content
    }

    fun <T> parseContent(content: String, cls: Class<T>) : T {
        return gson.fromJson<T>(content, cls)
    }
}