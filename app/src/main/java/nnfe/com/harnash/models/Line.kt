package nnfe.com.harnash.models

import com.google.gson.internal.LinkedTreeMap

data class Line(val line: String, val from: String, val to: String) {
    constructor(map: LinkedTreeMap<String, Any>) : this(
        map["line"] as String,
        map["from"] as String,
        map["to"] as String
    )

    override fun toString(): String {
        return line + ":" + from + ":" + to
    }
}