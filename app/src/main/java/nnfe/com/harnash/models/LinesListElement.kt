package nnfe.com.harnash.models
import kotlin.collections.ArrayList


class LinesListElement(
    val lineNumber: String,
    val selectedRoutes: ArrayList<Any>
) {
    companion object {
        fun convertLinePreferenceToLinesList(linePreferences: List<LinePreference>) : ArrayList<Any> {
            val linesList = ArrayList<Any>()
            for (lp in linePreferences) {
                var lineListEl : LinesListElement? = null
                for (lle in linesList) {
                    lle as LinesListElement
                    if(lle.lineNumber.compareTo(lp.line) == 0) {
                        lineListEl = lle
                        break
                    }
                }

                if(lineListEl == null) {
                    lineListEl = LinesListElement(lp.line, ArrayList())
                    linesList.add(lineListEl)
                }

                var pair : Pair<String, ArrayList<String>>? = null
                for (bs in lineListEl.selectedRoutes) {
                    bs as Pair<String, ArrayList<String>>
                    if(lp.busStation.compareTo(bs.first) == 0) {
                        pair = bs
                        break
                    }
                }

                if(pair == null) {
                    pair = Pair(lp.busStation, ArrayList())
                    lineListEl.selectedRoutes.add(pair)
                }

                var alreadyExists = false

                var time = String()
                if(lp.hour.toString().length == 1)
                    time += "0"
                time += lp.hour.toString() + ":"
                if(lp.minute.toString().length == 1)
                    time += "0"
                time += lp.minute.toString()

                for (t in pair.second) {
                    if(t.compareTo(time) == 0)
                        alreadyExists = true
                }

                if(!alreadyExists)
                    pair.second.add(time)

            }

            return linesList
        }
    }
}