package nnfe.com.harnash.models

import com.google.gson.internal.LinkedTreeMap

data class LinePreference(
    var line: String,
    var hour: Int,
    var minute: Int,
    var busStation: String
) {
    constructor(map : LinkedTreeMap<String, Any>) : this(
        map["line"] as String,
        (map["hour"] as Double).toInt(),
        (map["minute"] as Double).toInt(),
        map["busStation"] as String)
}