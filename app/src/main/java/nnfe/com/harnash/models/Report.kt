package nnfe.com.harnash.models

import com.google.android.gms.maps.model.LatLng

import com.google.gson.internal.LinkedTreeMap

class Report(
    var transportNumber: String,
    var reportType: ReportType,
    var delay: Int?,
    var numberOfReports: Int,
    var location: LatLng,
    var from: String,
    var to: String,
    var stop: String,
    var hour: Int,
    var minute: Int
) {

    constructor(map : LinkedTreeMap<String, Any>) : this(
        map["transportNumber"] as String,
        ReportType.valueOf(map["reportType"] as String),
        (map["delay"] as Double).toInt(),
        (map["numberOfReports"] as Double).toInt(),
        LatLng(map["lat"] as Double, map["lng"] as Double),
        (map["from"] as String),
        (map["to"] as String),
        (map["stop"] as String),
        (map["hour"] as Double).toInt(),
        (map["minute"] as Double).toInt())

    fun createReportSendModel() : ReportSendModel {
        return ReportSendModel(transportNumber, hour, minute, stop, reportType, delay as Int, from, to)
    }
}