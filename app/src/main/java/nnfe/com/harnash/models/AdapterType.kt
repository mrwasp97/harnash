package nnfe.com.harnash.models

enum class AdapterType {
    LinesList,
    BusStationsList,
    BusArriveTimeList
}