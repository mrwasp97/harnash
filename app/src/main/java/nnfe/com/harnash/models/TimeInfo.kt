package nnfe.com.harnash.models

import com.google.gson.internal.LinkedTreeMap
import java.lang.StringBuilder

class TimeInfo(val hour: Int, val minute: Int) {
    constructor(map : LinkedTreeMap<String, Any>) : this(
        (map["hour"] as Double).toInt(),
        (map["minute"] as Double).toInt())

    override fun toString(): String {
        return getTimeString(hour, minute)
    }

    fun getTimeString(hour: Int, minute: Int) : String {
        val returnString = java.lang.StringBuilder()
        if(hour < 10) {
            returnString.append("0" + hour.toString())
        } else {
            returnString.append(hour.toString())
        }
        returnString.append(":")
        if(minute < 10) {
            returnString.append("0" + minute.toString())
        } else {
            returnString.append(minute.toString())
        }
        return returnString.toString()
    }
}