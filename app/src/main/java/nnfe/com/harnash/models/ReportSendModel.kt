package nnfe.com.harnash.models

data class ReportSendModel (
    var line: String = "",
    var hour: Int = -1,
    var minute: Int = -1,
    var stop: String = "",
    var reportType: ReportType = ReportType.Accident,
    var delay: Int = -2,
    var from: String = "from",
    var to: String = "to"
)