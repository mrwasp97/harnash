package nnfe.com.harnash.models

enum class ReportType {
    Accident,
    Delay
}