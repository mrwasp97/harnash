package nnfe.com.harnash.models

import com.google.gson.internal.LinkedTreeMap

data class Stop(private val name: String) {
    constructor(map: LinkedTreeMap<String, Any>) : this(
        map["name"] as String
    )

    override fun toString(): String {
        return name
    }
}