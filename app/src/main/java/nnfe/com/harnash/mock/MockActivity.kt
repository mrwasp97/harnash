package nnfe.com.harnash.mock

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.Toast
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.iid.FirebaseInstanceId
import nnfe.com.harnash.R
import nnfe.com.harnash.controllers.MainController
import nnfe.com.harnash.models.LinePreference

class MockActivity : AppCompatActivity() {

    private lateinit var controller: MainController

    private val mockPreference = LinePreference("17", 10, 30, "Koszykowa 01")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_mock)
        setupController()
    }

    private fun bindFuncToButton(func: ()->Unit, id: Int) {
        val button = findViewById<Button>(id)
        button.setOnClickListener {
            func()
        }
    }

    private fun setupController() {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w("Token: ", "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                val token = task.result?.token

                if (token != null) {
                    controller = MainController(this, token)
                } else {
                    Toast.makeText(this, "Fucking error, token not found!!!!", Toast.LENGTH_SHORT).show()
                }
            })
    }
}
